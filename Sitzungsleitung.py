import discord
from discord.ext import commands
from dotenv import load_dotenv
load_dotenv()

import os
token = os.environ.get("TOKEN")

from datetime import datetime



intents = discord.Intents.default()
intents.members = True #this shows as a pylint error but it's working
bot = commands.Bot(command_prefix='.', intents=intents)

""" helper functions """

#print ready in terminal
@bot.event
async def on_ready():
    print ('We have logged in as {0.user}'.format(bot))
    await bot.change_presence(activity=discord.Game("Das einzig arbeitende Vorstandsmitglied."))

#prints bot ping
@bot.command()
async def ping(ctx):
    await ctx.send(f'Pong! {round(bot.latency*1000)} ms')


# clear amount of messages in channel
@bot.command()
@commands.has_role("@admin")
async def clear(ctx, amount):
    await ctx.channel.purge(limit = int(amount))
""" end helper functions """

""" start section redeliste """
talkinglist = []

#add user to talkinglist if not in list
@bot.command()
async def meldung(ctx):
    # local variable to note if user is in list 
    inlist = False
    # loop through talkinglist and check for user id
    for x in talkinglist:
        if ctx.author.id not in x:
            pass
        else:
            #if user id in list set inlist to true and break loop
            inlist = True
            break
    #if not inlist after looping through talkinglist add id and name and tell user his position in line
    if not inlist:
        talkinglist.append((ctx.author.id, ctx.author.display_name))
        await ctx.send("Hey {}, du bist jetzt an {}. Stelle auf der Redeliste.".format(ctx.author.mention, len(talkinglist)))
    else:
        await ctx.send("Hey {}, du stehst bereits auf der Liste.".format(ctx.author.mention))
    print(talkinglist)
    
#print current talkinglist
@bot.command()
@commands.has_any_role("Redeleitung", "@admin")
async def redeliste(ctx):
    await bot.wait_until_ready()
    print(talkinglist)
    #if there are entries in talkinglist display list as discord embed with fString
    if talkinglist:
        embed = discord.Embed()
        fields = [("Redeliste", "\n".join([f"{idx}. {x[1]}" for idx, x in enumerate(talkinglist, start=1)]), False)]
        for name, value, inline in fields:
            embed.add_field(name=name, value=value, inline=inline)
        await ctx.send(embed=embed)
    #if list is empty call emptylist
    else:
        await emptylist(ctx)

@bot.command()
@commands.has_any_role("Redeleitung", "@admin")
async def next(ctx):
    if talkinglist:
        await ctx.send("Hey {}, du sprichst jetzt.".format(bot.get_user(talkinglist.pop(0)[0]).mention))
    #if list is empty call emptylist
    else:
        await emptylist(ctx)

# helper function to not repeat msgs in different functions
async def emptylist(ctx):
    await ctx.send('Die Redeliste ist leer.')

""" end section redeliste """

""" start section user role management """

#give a member a role if the current user calling it is an adming
@bot.command()
@commands.has_role("@admin")
async def addrole(ctx, member: discord.Member, role:discord.Role):
    await member.add_roles(role)
    await ctx.send(f'Hey {ctx.author.mention}, {member.display_name} hat nun die Rolle {role}.')

#error handling if either the member or the role doesn't exist or is wrong
@addrole.error
async def addrole_error(ctx, error):
    if isinstance(error, commands.BadArgument):
        await ctx.send('Der User oder die Rolle existieren nicht.')

#if user is admin delete role of a user
@bot.command()
@commands.has_role("@admin")
async def delrole(ctx, member: discord.Member, role: discord.Role):
    await member.remove_roles(role)
    await ctx.send(f'Hey {ctx.author.mention}, {member.display_name} hat die Rolle {role} nicht mehr.')

#error handling if either the member or role doesn't exist or is wrong
@delrole.error
async def delrole_error(ctx, error):
    if isinstance(error, commands.BadArgument):
        await ctx.send('Diese Aktion kann nicht ausgeführt werden.')   

""" end user role management """


""" start section poll functions """
#list of letter emojis for polls
options =["🇦","🇧","🇨","🇩","🇪","🇫","🇬","🇭","🇮","🇯","🇰","🇱","🇲","🇳","🇴","🇵","🇶","🇷","🇸","🇹","🇺","🇻","🇼","🇽","🇾","🇿"]
poll_msg = [] #todo get rid of list and global keywords, refactor to class?


#our very own poll bot
@bot.command()
async def poll(ctx, question, *answers):
    if len(answers) < 2:
        await ctx.send('Diese Poll hat nicht genug Antworten.')
    elif len(answers) > 26:
        await ctx.send('Es sind leider nur 26 Antwortmöglichkeiten erlaubt.')
    else:
        #creates embed for poll question and reactions
        embed = discord.Embed(title = "📊"+question, timestamp=datetime.utcnow())
        fields = [("Antworten", "\n".join([f"{options[idx]} {answer}" for idx, answer in enumerate(answers)]), False)]
        embed.set_footer(text='Abstimmung erstellt von {}'.format(ctx.author.display_name))

        for name, value, inline in fields:
            embed.add_field(name=name, value=value, inline=inline)
        
        #adds emoji reactions for answers
        await ctx.send("Bitte gebt jetzt eure Stimmen ab.") # todo get bot to mention @here to alert everyone not idle
        msg = await ctx.send(embed=embed)

        for emoji in options[:len(answers)]:
            await msg.add_reaction(emoji)

        # create global poll_msg list to save poll data and close/post results later
        global poll_msg
        poll_msg = [msg.id, question, *answers]
        

#close poll and post results
@bot.command()
async def close(ctx):
    global poll_msg
    if poll_msg:
        msg = await ctx.fetch_message(poll_msg[0])
        #creates embed for poll results 
        embed = discord.Embed(title = "Ergebnis für 📊 \""+poll_msg[1]+"\"",  timestamp=datetime.utcnow())
        #creates fields for poll answers and adds number of reactions minus bot reaction 
        fields = [("Antworten", "\n".join([f"{answer}: {msg.reactions[idx].count-1}" for idx, answer in enumerate(poll_msg[2:])]), False)]
        embed.set_footer(text='Poll geschlossen von {}'.format(ctx.author.display_name))

        for name, value, inline in fields:
            embed.add_field(name=name, value=value, inline=inline)
        
        #send poll result, delete poll, reset global poll_msg variable to None as there is no longer an active poll 
        await ctx.send(embed=embed)
        await msg.delete()
        poll_msg = []
    else:
        await ctx.send("Es gibt keine aktive Poll!")

# get active poll in fast chat
@bot.command()
async def activepoll(ctx):
    # if there is an active poll, reply to the poll post
    if poll_msg:
        msg = await ctx.fetch_message(poll_msg[0])
        await msg.reply("Hier ist die aktive Poll!")
    else: 
        await ctx.send("Es gibt keine aktive Poll!")

""" end section poll functions """


""" general bot functionality and msg reactions """
#on_message functionality for specific messages
@bot.event
async def on_message(message):
    #checks for commands first before checking for specific message content
    await bot.process_commands(message)

    if message.author == bot.user:
        return

    if message.content.startswith('.Hallo'):
        await message.channel.send(f'Hallo, {message.author.mention}! ')
        return

    #deletes command messages in all channels
    async for message in message.channel.history():
        if message.content.startswith('.'):
            await message.delete()

""" end general functionality """

bot.run(token)