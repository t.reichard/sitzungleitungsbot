# Sitzungleitungsbot

Ein Projekt der ADW, um einen Discord-Python-Bot zu erstellen, der es ermöglicht Gruppensitzungen auf dem eigenen Discord-Server zu managen. 

<hr>

## Anleitung
under construction

<hr>

## Build Status

early development

<hr>

## Funktionalitäten:
 Alle Commands beginnen mit 
 >.

- Hallo 
    - .Hallo

- Ping
    - .ping

- Rollenmanagement 
    - .addrole User Role
    - .delrole User Role

- Poll
    - .poll "Frage" Antwort1 - Antwort26
    - .activepoll 
    - .close 

- Redeliste
    - .meldung 
    - .redeliste
    - .next

- Chatmanagement
    - .clear Anzahl

<hr>

### Planned
- Redeliste
    - direkt dazu?
    - öffnen / schließen?
    - nur für spezifische Channel?
- geheime Abstimmungen
- Chatmanagement
    - ~~clear~~
    - timeout
    - mute
- Hilfefunktion
    - für Commands
    - für spezifische Commands
- Links (db-Anbindung)
    - aktuelle Sitzungsfolien
    - letztes Protokoll
    - bestimmte Umfragen etc?
- ??interaktiv Commands erstellen?? (db-anbindung?)

### Done
- basic Redeliste
- Rollenmanagement (add/delete)
- Polls (create)
- aktive Poll anzeigen
- Poll schließen und Ergebnis posten
- Ping
- Hallo

<hr>

Contributors:
- Thiemo Reichard

Hilfe und Unterstützung gerne gesehen.